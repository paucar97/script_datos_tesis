import pandas as pd
NOMBRE_TABLA_DB_PROTEIN = "DB_PROTEIN"
NOMBRE_TABLA_DB_ENTREZ = "DB_ENTREZ"
NOMBR_TABLA_DB_UNIPROT = "DB_SEQUENCE_INFO"
CONTADOR_DB_PROTEIN = 1
CONTADOR_DB_ENTREZ = 1
CONTADOR_DB_INTERACTION = 1

def insert_db_protein(index):
    protein_name = df_proteina.iloc[index,4]
    insert = f'INSERT INTO {NOMBRE_TABLA_DB_PROTEIN} VALUES ({CONTADOR_DB_PROTEIN},"{protein_name}",1);'
    
    print(insert)

def insert_db_entrez(index):
    id_entrez = df_proteina.iloc[index, 2]
    global CONTADOR_DB_ENTREZ
    if id_entrez == id_entrez: #no es nulo
        insert = f'INSERT INTO {NOMBRE_TABLA_DB_ENTREZ} VALUES ({CONTADOR_DB_ENTREZ},{int(id_entrez)},{CONTADOR_DB_PROTEIN});'
        CONTADOR_DB_ENTREZ += 1
        print(insert)

def insert_db_uniprot(index):
    entry_name = df_proteina.iloc[index, 0]
    gene_symbol = df_proteina.iloc[index,1]
    insert = f'INSERT INTO {NOMBR_TABLA_DB_UNIPROT} VALUES ("{entry_name}","{gene_symbol}",{CONTADOR_DB_PROTEIN},1);'
    print(insert)

df_proteina = pd.read_excel('proteins_download.xlsx',usecols= [1,2,3,4,5])


for i in range(len(df_proteina)):
    insert_db_protein(i)
    insert_db_entrez(i)
    insert_db_uniprot(i)
    CONTADOR_DB_PROTEIN += 1