import requests, sys



class Utils_unitprot:

    def get_gene(accession):
        print(accession)
        requestURL = "https://www.ebi.ac.uk/proteins/api/proteins?offset=0&size=100&accession={}".format(str(accession))
        r = requests.get(requestURL, headers={ "Accept" : "application/json"})
        if not r.ok:
            r.raise_for_status()
            sys.exit()
        

        responseBody = r.json()
        #print(responseBody)
        print(responseBody[0].get('id',None))
        return responseBody[0].get('id',None)

    def get_gene_from_df(accession, df):
        dfAux = df[df['Entry'] == accession]
        if len(dfAux) == 0:
            return None
        entry_name = dfAux.iloc[0,1]
        #print(entry_name)
        return entry_name
