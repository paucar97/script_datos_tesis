import mygene
mg = mygene.MyGeneInfo()

class Utils_Entrez:
    def get_entrez_id(gene):
        out = mg.querymany([gene], scopes='symbol', fields='entrezgene', species='human')
        print(out)
        return out[0].get('entrezgene',None)

    def get_entrez_id_from_list(l_gene):
        out = mg.querymany(l_gene, scopes='symbol', fields='entrezgene', species='human')
        #print(out)
        return out

    def get_entrez_id_from_df(gene,df):
        dfAux = df[df['query'] == gene]
        if len(dfAux) == 0:
            return None
        id = dfAux.iloc[0,1]
        #print(entry_name)
        if type(id) == str:
            return id
        return None

#r = Utils_Entrez.get_entrez_id('Gm10494')
#print("Final" , r)
