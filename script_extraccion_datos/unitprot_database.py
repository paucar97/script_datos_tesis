import pandas as pd
from utils_unitprot import Utils_unitprot
from utils_entrez import Utils_Entrez

def remove_duplicate(x):
  return list(dict.fromkeys(x))

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Print New Line on Complete
    if iteration == total: 
        print()
def clean_protein_interaction(x,default_itself):
    aux = (x.replace(' ','')).split('-')[0]
    if aux == 'Itself':
        aux = default_itself
    return aux


df_protein =  pd.read_excel('uniprot_columnas_interaccione_crossReference.xlsx')

l_protein_entry_name = []
#l_protein_gene_id_entrez = []
l_protein_gene_name = []
l_protein_name = []
l_protein_interactions = [] # Gene name 
printProgressBar(0, len(df_protein), prefix = 'Progress:', suffix = 'Complete', length = 50)
for i in range(len(df_protein)):
    accession_itself = df_protein.iloc[i,0]
    protein_entry_name = df_protein.iloc[i,1] # protein name
    protein_name = df_protein.iloc[i,3] # Protein name (complete)
    if type(df_protein.iloc[i,4]) == float:
        continue
    protein_gene_name = df_protein.iloc[i,4].split(' ')[0].replace(' ','')
    protein_interactions = df_protein.iloc[i,6]
    if type(protein_interactions) == str:
        protein_interactions = protein_interactions.split(';')
        protein_interactions = [ clean_protein_interaction(x,accession_itself) for x in protein_interactions ]
        protein_interactions = remove_duplicate(protein_interactions)
        protein_interactions = [ Utils_unitprot.get_gene_from_df(x,df_protein) for x in protein_interactions]

    else:
        protein_interactions = None

    l_protein_entry_name.append(protein_entry_name)
    l_protein_gene_name.append(protein_gene_name)
    l_protein_interactions.append(protein_interactions)
    l_protein_name.append(protein_name)
    printProgressBar(i + 1, len(df_protein), prefix = 'Progress:', suffix = 'Complete', length = 50)

out = Utils_Entrez.get_entrez_id_from_list(l_protein_gene_name)
df_entrez_id = pd.DataFrame(out)
df_entrez_id = df_entrez_id.dropna(subset=['entrezgene'])
df_entrez_id.to_excel('entrez_id.xlsx')
l_protein_gene_id_entrez = [Utils_Entrez.get_entrez_id_from_df(gene,df_entrez_id) for gene in l_protein_gene_name]

data = {
    'Entry Name' : l_protein_entry_name,
    'Gene Name' : l_protein_gene_name,
    'Entrez id': l_protein_gene_id_entrez,
    'Protein Interactions' : l_protein_interactions,
    'Protein Name' : l_protein_name
}
df_proteins_download = pd.DataFrame(data)
df_proteins_download.to_excel('proteins_download.xlsx')